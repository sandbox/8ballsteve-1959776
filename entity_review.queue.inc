<?php

/**
 * implementation of hook_cron().
 */
function entity_review_cron() {

  // use DrupalQueue to handle batch processing during cron
  $queue = DrupalQueue::get('entity_review');
  $content = entity_review_get_data();

  if ($content) {
    foreach ($content as $record) {

      // allow other modules to define if notification should be sent
      $data = module_invoke_all('entity_review_process_record', $record);

      // modules are required to add skip = TRUE to the returned array
      if (!isset($data['skip'])) {
        // add item to queue
        // if notification not already sent
        if (!_entity_review_notification_sent($record)) {
          // add to queue
          $queue->createItem($record);
        }
      }
    }
  }
}

/**
 * implementation of hook_cron_queue_info()
 */
function entity_review_cron_queue_info() {
  $queues = array();
  $queues['entity_review'] = array(
    'worker callback' => 'entity_review_process_queue',
    'time' => 120,
  );
  return $queues;
}

/**
 * Process queue of nodes
 */
function entity_review_process_queue($record) {
  //watchdog('entity_review', 'processing %nid - record: %record', array('%nid' => $record['nid'], '%record' => serialize($record)), WATCHDOG_DEBUG);
  entity_review_sendmail($record['nid'], $record['uid'], $record['review_date']);
}

